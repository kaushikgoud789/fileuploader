# Using openjdk version 8 as base image
FROM openjdk:8

WORKDIR "/fileuploader"

# Creating a temporary volume directory, this is used to store logs
VOLUME /tmp

# Retriving the latest jar from nexus and copying into the docker container
ADD target/fileuploader-0.0.1-SNAPSHOT.jar fileuploader-0.0.1-SNAPSHOT.jar


# Running the app
ENTRYPOINT ["sh","-c","exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar fileuploader-0.0.1-SNAPSHOT.jar"]
