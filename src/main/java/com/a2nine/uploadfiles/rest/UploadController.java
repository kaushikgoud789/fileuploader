package com.a2nine.uploadfiles.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.a2nine.uploadfiles.service.StorageService;

@Controller
public class UploadController {

	@Autowired
	StorageService storageService;

	List<String> files = new ArrayList<String>();

	@PostMapping("/{transactionNumber}")
	public ResponseEntity<String> uploadTransactionFiles(@PathVariable("transactionNumber") String transactionNumber,
			@RequestParam("file") List<MultipartFile> file) {
		String message = "";
		try {
			storageService.uploadTransactionToAWSS3(file, transactionNumber);
			// files.add(file.getOriginalFilename());

			message = "You successfully uploaded "
					// file.getOriginalFilename()
					+ "!";
			return ResponseEntity.status(HttpStatus.OK).body(message);
		} catch (Exception e) {
			message = "FAIL to upload "
					// file.getOriginalFilename()
					+ "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		}
	}

	@PostMapping("/contact/{contactId}")
	public ResponseEntity<String> uploadContactFiles(@PathVariable("contactId") String contactId,
			@RequestParam("file") List<MultipartFile> file) {
		String message = "";
		try {
			storageService.uploadContactToAWSS3(file, contactId);
			// files.add(file.getOriginalFilename());

			message = "You successfully uploaded "
					// file.getOriginalFilename()
					+ "!";
			return ResponseEntity.status(HttpStatus.OK).body(message);
		} catch (Exception e) {
			message = "FAIL to upload "
					// file.getOriginalFilename()
					+ "!";
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
		}
	}

	@GetMapping("/getallfiles")
	public ResponseEntity<List<String>> getListFiles(String transactionNumber) {
		return ResponseEntity.ok().body(storageService.downloadFromS3(transactionNumber));
	}

}
