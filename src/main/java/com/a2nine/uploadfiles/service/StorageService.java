package com.a2nine.uploadfiles.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ListObjectsV2Result;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;

@Service
public class StorageService {

	Logger log = LoggerFactory.getLogger(this.getClass().getName());

	@Value("${aws.s3.access.key}")
	private String accessKey;

	@Value("${aws.s3.secret.key}")
	private String secretKey;

	@Value("${aws.s3.bucket.name}")
	private String bucketName;

	@Value("${aws.s3.contact.folder.name}")
	private String contactFolder;

	@Value("${aws.s3.url.prefix}")
	private String urlPrefix;

	public void uploadTransactionToAWSS3(List<MultipartFile> mFile, String transactionNumber) throws IOException {
		for (MultipartFile file : mFile) {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(file.getSize());

			AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
			AmazonS3 s3client = new AmazonS3Client(credentials);
			PutObjectRequest putObjectRequest = new PutObjectRequest(this.bucketName,
					transactionNumber + "/" + file.getOriginalFilename(), file.getInputStream(), metadata)
							.withCannedAcl(CannedAccessControlList.PublicRead);
			// send request to S3 to create folder
			s3client.putObject(putObjectRequest);
		}

	}

	public void uploadContactToAWSS3(List<MultipartFile> mFile, String transactionNumber) throws IOException {
		for (MultipartFile file : mFile) {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(file.getSize());

			AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
			AmazonS3 s3client = new AmazonS3Client(credentials);
			PutObjectRequest putObjectRequest = new PutObjectRequest(this.bucketName,
					contactFolder + "/" + transactionNumber + "/" + file.getOriginalFilename(), file.getInputStream(),
					metadata).withCannedAcl(CannedAccessControlList.PublicRead);
			// send request to S3 to create folder
			s3client.putObject(putObjectRequest);
		}

	}

	public List<String> downloadFromS3(String transactionNumber) {
		List<String> urls = new ArrayList<>();
		String url = "";
		AWSCredentials credentials = new BasicAWSCredentials(this.accessKey, this.secretKey);
		AmazonS3 s3client = new AmazonS3Client(credentials);
		ListObjectsV2Result objectsInBucket = s3client.listObjectsV2(this.bucketName + "/" + transactionNumber);
		for (S3ObjectSummary s : objectsInBucket.getObjectSummaries()) {
			System.out.println("s.getKey() = " + s.getKey());
			url = new String(this.urlPrefix + this.bucketName + "/" + s.getKey());
			urls.add(url);
		}
		return urls;
	}
}
